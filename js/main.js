'use strict'

let bounceAnimate = document.getElementById('#bounceInLeft')
let clone = bounceAnimate.cloneNode(true);
clone.classList.add('fixAnim');
const Result = e => {
    if(bounceAnimate.getBoundingClientRect().bottom < 0) bounceAnimate.parentElement.append(clone);
    else clone.remove();
};

window.addEventListener('scroll', Result);